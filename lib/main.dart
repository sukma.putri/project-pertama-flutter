import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          leading: Icon(Icons.auto_awesome_mosaic),
          title: Text('Biodata Mahasiswa Undiksha'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.thumb_up),
              onPressed: () {},
            ), // IconButton
            IconButton(
              icon: Icon(Icons.thumb_down),
              onPressed: () {},
            ), // IconButton
          ], // <Widget> []
          elevation: 0,
          backgroundColor: Colors.black26,
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            children: [
              Image(
                image: AssetImage('assets/IMG_2546.JPG'),
                width: 100.0,
                height: 100.0,
              ),
              Text('Sukma Putri Novianti',
                  style: TextStyle(
                    fontSize: 21,
                    fontFamily: "Times New Romans",
                    height: 2.0,
                  )),
              Text('1915051001'),
              Text('PTI 4 A'),
              Text('Pendidikan Teknik Informatika'),
              Text('Universitas Pendidikan Ganesha'),
            ],
          ),
        ),
      ),
    ),
  );
}
